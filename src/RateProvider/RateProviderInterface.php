<?php
namespace Astartsky\RateFetcher\RateProvider;

use Astartsky\RateFetcher\Rate;
use SebastianBergmann\Money\Currency;

interface RateProviderInterface
{
    /**
     * @param Currency $source
     * @param Currency $currency
     * @param \DateTime $date
     * @return Rate
     */
    public function fetch(Currency $source, Currency $currency, \DateTime $date);
} 
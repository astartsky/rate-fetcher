<?php
namespace Astartsky\RateFetcher\RateProvider;

use Astartsky\RateFetcher\Rate;
use Astartsky\RateFetcher\RateException;
use SebastianBergmann\Money\Currency;

class RbkRateProvider implements RateProviderInterface
{
    protected $maxVacationDays;

    /**
     * @param int $maxVacationDays
     */
    public function __construct($maxVacationDays = 5)
    {
        $this->maxVacationDays = $maxVacationDays;
    }

    /**
     * @param Currency $source
     * @param Currency $currency
     * @param \DateTime $date
     * @return string
     * @throws \Astartsky\RateFetcher\RateException
     */
    protected function makeRequest(Currency $source, Currency $currency, \DateTime $date)
    {
        $url = sprintf("http://quote.rbc.ru/cgi-bin/conv/data/history/%s/%s/cb.0/%s/", $source->getCurrencyCode(), $currency->getCurrencyCode(), $date->format('d.m.Y'));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        if (!$content) {
            throw new RateException(sprintf("Transfer error: %s", curl_error($ch)));
        }
        curl_close($ch);

        return $content;
    }

    /**
     * Fetch rate for specified day
     * Gate can give no values for bank vacation days
     *
     * @param Currency $source
     * @param Currency $currency
     * @param \DateTime $date
     * @return Rate
     * @throws RateException
     */
    public function fetch(Currency $source, Currency $currency, \DateTime $date)
    {
        $response = $this->makeRequest($source, $currency, $date);

        $rate = null;
        $count = 0;
        $rateDate = clone $date;
        do {
            if (preg_match('#'.preg_quote($rateDate->format('d.m.Y')).',([0-9.]*)"#iUs', $response, $res)) {
                $rate = $res[1];
            } else {
                $rateDate->modify("-1 day");
                $count++;
            }
        } while (is_null($rate) && $count < $this->maxVacationDays);

        if (is_null($rate)) {
            throw new RateException(sprintf("No rates for %s days before date", $this->maxVacationDays));
        }

        return new Rate($source, $currency, $rateDate, $rate);
    }
}
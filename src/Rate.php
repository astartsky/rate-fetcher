<?php
namespace Astartsky\RateFetcher;

use SebastianBergmann\Money\Currency;

class Rate
{
    protected $source;
    protected $target;
    protected $date;


    protected $rate;

    /**
     * @param Currency $source
     * @param Currency $target
     * @param \DateTime $date
     * @param double $rate
     */
    public function __construct(Currency $source, Currency $target, \DateTime $date, $rate)
    {
        $this->source = $source;
        $this->target = $target;
        $this->date = $date;
        $this->rate = $rate;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @return \SebastianBergmann\Money\Currency
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return \SebastianBergmann\Money\Currency
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
} 
<?php
namespace Astartsky\RateFetcher;

use Astartsky\RateFetcher\RateProvider\RateProviderInterface;
use SebastianBergmann\Money\Currency;

class RateFetcher
{
    /** @var RateProviderInterface */
    protected $provider;

    /**
     * @param RateProviderInterface $rateProvider
     */
    public function registerRateProvider(RateProviderInterface $rateProvider)
    {
        $this->provider = $rateProvider;
    }

    /**
     * @param Currency $source
     * @param Currency $currency
     * @param \DateTime $date
     * @return Rate
     * @throws RateException
     */
    public function fetch(Currency $source, Currency $currency, \DateTime $date)
    {
        if (!$this->provider instanceof RateProviderInterface) {
            throw new RateException("Rate provider is not set");
        }

        try {
            $rate = $this->provider->fetch($source, $currency, $date);
        } catch (\Exception $e) {
            throw new RateException("Problem during rate fetch", 0, $e);
        }

        return $rate;
    }
} 
<?php
namespace Astartsky\RateFetcher\Silex;

use Astartsky\RateFetcher\RateFetcher;
use Astartsky\RateFetcher\RateProvider\RbkRateProvider;
use Silex\Application;
use Silex\ServiceProviderInterface;

class RateFetcherServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     */
    public function register(Application $app)
    {
        $app['rate_fetcher'] = $app->share(function() {
            return new RateFetcher();
        });

        $app['rate_fetcher.provider.rbk'] = $app->share(function() {
            return new RbkRateProvider();
        });

        $app['rate_fetcher'] = $app->extend('rate_fetcher', function($rateFetcher) use ($app) {
            /** @var $rateFetcher RateFetcher */
            $rateFetcher->registerRateProvider($app['rate_fetcher.provider.rbk']);
            return $rateFetcher;
        });
    }

    /**
     * Bootstraps the application.
     */
    public function boot(Application $app)
    {

    }
}